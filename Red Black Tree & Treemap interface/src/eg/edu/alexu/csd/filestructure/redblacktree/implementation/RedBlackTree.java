package eg.edu.alexu.csd.filestructure.redblacktree.implementation;

import java.util.Random;

import javax.management.RuntimeErrorException;

import eg.edu.alexu.csd.filestructure.redblacktree.INode;
import eg.edu.alexu.csd.filestructure.redblacktree.IRedBlackTree;

public class RedBlackTree<T extends Comparable<T>, V> implements IRedBlackTree<T, V> {

	private class Node<Key extends Comparable<Key>, Value> implements INode<Key, Value> {
		private INode<Key, Value> parent;
		private INode<Key, Value> rightChild;
		private INode<Key, Value> leftChild;
		private Key key;
		private Value value;
		// black is false ,red is true
		private boolean color;

		@Override
		public void setParent(INode<Key, Value> parent) {
			this.parent = parent;
		}

		@Override
		public INode<Key, Value> getParent() {
			return parent;
		}

		@Override
		public void setLeftChild(INode<Key, Value> leftChild) {
			this.leftChild = leftChild;
		}

		@Override
		public INode<Key, Value> getLeftChild() {
			return leftChild;
		}

		@Override
		public void setRightChild(INode<Key, Value> rightChild) {
			this.rightChild = rightChild;

		}

		@Override
		public INode<Key, Value> getRightChild() {
			return rightChild;
		}

		@Override
		public Key getKey() {
			return key;
		}

		@Override
		public void setKey(Key key) {
			this.key = key;
		}

		@Override
		public Value getValue() {
			return value;
		}

		@Override
		public void setValue(Value value) {
			this.value = value;
		}

		@Override
		public boolean getColor() {
			return color;
		}

		@Override
		public void setColor(boolean color) {
			this.color = color;

		}

		@Override
		public boolean isNull() {
			return this == nil;
		}

	}

	private INode<T, V> nil = new Node<>();
	private INode<T, V> root = nil;

	@Override
	public INode<T, V> getRoot() {
		return root;
	}

	@Override
	public boolean isEmpty() {
		return root == nil;
	}

	@Override
	public void clear() {
		nil = new Node<>();
		root = nil;
	}

	private INode<T, V> getNode(T key) {
		INode<T, V> x = root;
		while (x != nil && key != null) {
			if (x.getKey().compareTo(key) > 0) {
				x = x.getLeftChild();
			} else if (x.getKey().compareTo(key) < 0) {
				x = x.getRightChild();
			} else {
				break;
			}
		}
		return x;
	}

	@Override
	public V search(T key) {
		if (key == null) {
			throw new RuntimeErrorException(null);
		}
		INode<T, V> x = getNode(key);
		return x != nil ? x.getValue() : null;
	}

	@Override
	public boolean contains(T key) {
		if (key == null) {
			throw new RuntimeErrorException(null);
		}
		return getNode(key) != nil;
	}

	@Override
	public void insert(T key, V value) {
		if (key == null || value == null) {
			throw new RuntimeErrorException(null);
		}
		INode<T, V> z = new Node<>();
		INode<T, V> y = nil;
		INode<T, V> x = root;
		while (x != nil) {
			y = x;
			if (x.getKey().compareTo(key) > 0) {
				x = x.getLeftChild();
			} else if (x.getKey().compareTo(key) < 0) {
				x = x.getRightChild();
			} else {
				x.setValue(value);
				return;
			}
		}
		if (y == nil) {
			root = z;
		} else if (y.getKey().compareTo(key) > 0) {
			y.setLeftChild(z);
		} else {
			y.setRightChild(z);
		}
		z.setParent(y);
		z.setLeftChild(nil);
		z.setRightChild(nil);
		z.setColor(INode.RED);
		z.setKey(key);
		z.setValue(value);
		insertFix(z);
	}

	private void insertFix(INode<T, V> z) {
		while (z.getParent().getColor() == INode.RED) {
			if (z.getParent().getParent().getLeftChild() == z.getParent()) {
				INode<T, V> uncle = z.getParent().getParent().getRightChild();
				if (uncle.getColor() == INode.RED) {
					uncle.setColor(INode.BLACK);
					z.getParent().setColor(INode.BLACK);
					z.getParent().getParent().setColor(INode.RED);
					z = z.getParent().getParent();
				} else {
					if (z.getParent().getRightChild() == z) {
						z = z.getParent();
						leftRotate(z);
					}
					z.getParent().setColor(INode.BLACK);
					z.getParent().getParent().setColor(INode.RED);
					rightRotate(z.getParent().getParent());
				}
			} else {
				INode<T, V> uncle = z.getParent().getParent().getLeftChild();
				if (uncle.getColor() == INode.RED) {
					uncle.setColor(INode.BLACK);
					z.getParent().setColor(INode.BLACK);
					z.getParent().getParent().setColor(INode.RED);
					z = z.getParent().getParent();
				} else {
					if (z.getParent().getLeftChild() == z) {
						z = z.getParent();
						rightRotate(z);
					}
					z.getParent().setColor(INode.BLACK);
					z.getParent().getParent().setColor(INode.RED);
					leftRotate(z.getParent().getParent());
				}
			}
		}
		root.setColor(INode.BLACK);
	}

	@Override
	public boolean delete(T key) {
		Random r =  new Random();
		if (r.nextInt(2) == 0) {
			return delete1(key);
		}else {
			return delete2(key);
		}		
	}

	public boolean delete1(T key) {

		INode<T, V> z = getNode(key);
		if (z == nil) {
			throw new RuntimeErrorException(null);
		}
		INode<T, V> y = z;
		INode<T, V> x;
		boolean yOriginalColor = y.getColor();
		if (z.getLeftChild() == nil) {
			x = z.getRightChild();
			transplant(z, x);
		} else if (z.getRightChild() == nil) {
			x = z.getLeftChild();
			transplant(z, x);
		} else {
			y = treeMin(z.getRightChild());
			yOriginalColor = y.getColor();
			x = y.getRightChild();
			if (y.getParent() == z) {
				x.setParent(y);
			} else {
				transplant(y, x);
				y.setRightChild(z.getRightChild());
				y.getRightChild().setParent(y);
			}
			transplant(z, y);
			y.setLeftChild(z.getLeftChild());
			y.getLeftChild().setParent(y);
			y.setColor(z.getColor());
		}
		if (yOriginalColor == INode.BLACK) {
			deleteFixup1(x);
		}
		return true;

	}

	private void deleteFixup1(INode<T, V> x) {
		while (x != root && x.getColor() == INode.BLACK) {
			if (x.getParent().getLeftChild() == x) {
				INode<T, V> w = x.getParent().getRightChild();
				if (w.getColor() == INode.RED) {
					x.getParent().setColor(INode.RED);
					w.setColor(INode.BLACK);
					leftRotate(x.getParent());
					w = x.getParent().getRightChild();
				}
				if (w.getLeftChild().getColor() == INode.BLACK && w.getRightChild().getColor() == INode.BLACK) {
					w.setColor(INode.RED);
					x = x.getParent();
				} else {
					if (w.getRightChild().getColor() == INode.BLACK) {
						w.getLeftChild().setColor(INode.BLACK);
						w.setColor(INode.RED);
						rightRotate(w);
						w = x.getParent().getRightChild();
					}
					w.setColor(x.getParent().getColor());
					x.getParent().setColor(INode.BLACK);
					w.getRightChild().setColor(INode.BLACK);
					leftRotate(x.getParent());
					x = root;
				}
			} else {
				INode<T, V> w = x.getParent().getLeftChild();
				if (w.getColor() == INode.RED) {
					x.getParent().setColor(INode.RED);
					w.setColor(INode.BLACK);
					rightRotate(x.getParent());
					w = x.getParent().getLeftChild();
				}
				if (w.getLeftChild().getColor() == INode.BLACK && w.getRightChild().getColor() == INode.BLACK) {
					w.setColor(INode.RED);
					x = x.getParent();
				} else {
					if (w.getLeftChild().getColor() == INode.BLACK) {
						w.getRightChild().setColor(INode.BLACK);
						w.setColor(INode.RED);
						leftRotate(w);
						w = x.getParent().getLeftChild();
					}
					w.setColor(x.getParent().getColor());
					x.getParent().setColor(INode.BLACK);
					w.getLeftChild().setColor(INode.BLACK);
					rightRotate(x.getParent());
					x = root;
				}
			}
		}
		x.setColor(INode.BLACK);
	}

	private void leftRotate(INode<T, V> x) {
		INode<T, V> y = x.getRightChild();
		x.setRightChild(y.getLeftChild());
		if (x.getRightChild() != nil) {
			x.getRightChild().setParent(x);
		}
		y.setParent(x.getParent());
		if (x.getParent() == nil) {
			root = y;
		} else if (x.getParent().getRightChild() == x) {
			x.getParent().setRightChild(y);
		} else {
			x.getParent().setLeftChild(y);
		}
		y.setLeftChild(x);
		x.setParent(y);
	}

	private void rightRotate(INode<T, V> x) {
		INode<T, V> y = x.getLeftChild();
		x.setLeftChild(y.getRightChild());
		if (x.getLeftChild() != nil) {
			x.getLeftChild().setParent(x);
		}
		y.setParent(x.getParent());
		if (x.getParent() == nil) {
			root = y;
		} else if (x.getParent().getRightChild() == x) {
			x.getParent().setRightChild(y);
		} else {
			x.getParent().setLeftChild(y);
		}
		y.setRightChild(x);
		x.setParent(y);
	}

	private void transplant(INode<T, V> oldNode, INode<T, V> newNode) {
		if (oldNode == root) {
			root = newNode;
		} else if (oldNode.getParent().getLeftChild() == oldNode) {
			oldNode.getParent().setLeftChild(newNode);
		} else {
			oldNode.getParent().setRightChild(newNode);
		}
		newNode.setParent(oldNode.getParent());
	}

	private INode<T, V> treeMin(INode<T, V> x) {
		INode<T, V> y = x;
		while (y.getLeftChild() != nil) {
			y = y.getLeftChild();
		}
		return y;
	}

	private boolean delete2(T key) {
		// search for the node
		INode<T, V> x = root;
		while (x != nil && x.getKey().compareTo(key) != 0) {
			if (x.getKey().compareTo(key) > 0) {
				x = x.getLeftChild();
			} else if (x.getKey().compareTo(key) < 0) {
				x = x.getRightChild();
			}
		}
		if (x == nil) {
			throw new RuntimeErrorException(null);
		}

		INode<T, V> dB;
		boolean originalColor = x.getColor();// original color of the node will be deleted
		if (x.getLeftChild() == nil) {
			dB = x.getRightChild();
			transplant(x, dB);
		} else if (x.getRightChild() == nil) {
			dB = x.getLeftChild();
			transplant(x, dB);
		} else {
			// get successor (min of rightChild)
			INode<T, V> y;
			y = x.getRightChild();
			while (y.getLeftChild() != nil) {
				y = y.getLeftChild();
			}
			// now we want to change x and y and delete x
			originalColor = y.getColor();
			dB = y.getRightChild();
			transplant(y, dB);
			transplant(x, y);
			y.setColor(x.getColor());
			y.setLeftChild(x.getLeftChild());
			x.getLeftChild().setParent(y);
			y.setRightChild(x.getRightChild());
			x.getRightChild().setParent(y);
		}
		if (originalColor == INode.BLACK) {
			deleteFix2(dB);
		}
		return true;
	}

	private void deleteFix2(INode<T, V> dB) {
		if (dB == root || dB.getColor() == INode.RED) {
			dB.setColor(INode.BLACK);
			return;
		}
		INode<T, V> sibling;
		if (dB.getParent().getLeftChild() == dB) {
			// dB at left
			sibling = dB.getParent().getRightChild();
			if (sibling.getColor() == INode.RED) {
				// sibling case: sibling is red
				leftRotate(dB.getParent());
				dB.getParent().setColor(INode.RED);
				sibling.setColor(INode.BLACK);
				deleteFix2(dB);
			} else if (sibling.getLeftChild().getColor() == INode.BLACK
					&& sibling.getRightChild().getColor() == INode.BLACK) {
				// parent case:all black except parent unknown (cannot rotate)
				sibling.setColor(INode.RED);
				deleteFix2(dB.getParent());
			} else {// sibling is children case : one of them is red at least
					// and we want to ensure that sibling.right is red
				if (sibling.getRightChild().getColor() == INode.BLACK) {
					// sibling.right is black and sibling.left is red
					sibling = sibling.getLeftChild();
					rightRotate(sibling.getParent());
					sibling.setColor(INode.BLACK);
					sibling.getRightChild().setColor(INode.RED);
				}
				// sibling.right is red for sure now
				sibling.setColor(dB.getParent().getColor());
				dB.getParent().setColor(INode.BLACK);
				sibling.getRightChild().setColor(INode.BLACK);
				leftRotate(dB.getParent());
			}
		} else {
			sibling = dB.getParent().getLeftChild();
			if (sibling.getColor() == INode.RED) {
				// sibling case: sibling is red
				rightRotate(dB.getParent());
				dB.getParent().setColor(INode.RED);
				sibling.setColor(INode.BLACK);
				deleteFix2(dB);
			} else if (sibling.getRightChild().getColor() == INode.BLACK
					&& sibling.getLeftChild().getColor() == INode.BLACK) {
				// parent case:all black except parent unknown (cannot rotate)
				sibling.setColor(INode.RED);
				deleteFix2(dB.getParent());
			} else {// sibling is children case : one of them is red at least
					// and we want to ensure that sibling.left is red
				if (sibling.getLeftChild().getColor() == INode.BLACK) {
					// sibling.left is black and sibling.right is red
					sibling = sibling.getRightChild();
					leftRotate(sibling.getParent());
					sibling.setColor(INode.BLACK);
					sibling.getLeftChild().setColor(INode.RED);
				}
				// sibling.left is red for sure now
				sibling.setColor(dB.getParent().getColor());
				dB.getParent().setColor(INode.BLACK);
				sibling.getLeftChild().setColor(INode.BLACK);
				rightRotate(dB.getParent());
			}
		}

	}

}
