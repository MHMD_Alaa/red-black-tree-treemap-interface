package eg.edu.alexu.csd.filestructure.redblacktree.implementation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import javax.management.RuntimeErrorException;

import eg.edu.alexu.csd.filestructure.redblacktree.INode;
import eg.edu.alexu.csd.filestructure.redblacktree.IRedBlackTree;
import eg.edu.alexu.csd.filestructure.redblacktree.ITreeMap;

public class TreeMap<T extends Comparable<T>, V> implements ITreeMap<T, V> {

	private class EntryImp<Key extends Comparable<Key>, Value>
			implements Comparable<Entry<Key, Value>>, Map.Entry<Key, Value> {
		Key key;
		Value value;

		public EntryImp(Key key, Value value) {
			this.key = key;
			this.value = value;
		}

		@Override
		public Key getKey() {
			return key;
		}

		@Override
		public Value getValue() {
			return value;
		}

		@Override
		public Value setValue(Value value) {
			Value old = this.value;
			this.value = value;
			return old;
		}

		@Override
		public int compareTo(Entry<Key, Value> o) {
			return key.compareTo(o.getKey());
		}

		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof Entry<?,?>)) {
				return false;
			}
			if (this.getKey().equals(((Entry<?, ?>) obj).getKey())
					&& this.getValue().equals(((Entry<?, ?>) obj).getValue())) {
				return true;
			}
			return false;
		}

	}

	private IRedBlackTree<T, V> redBlackTree = new RedBlackTree<>();
	private int size;

	@Override
	public Entry<T, V> ceilingEntry(T key) {
		if (key == null) {
			throw new RuntimeErrorException(null);
		}
		INode<T, V> x = redBlackTree.getRoot();
		while (!x.isNull()) {
			if (key.compareTo(x.getKey()) > 0) {
				if (x.getRightChild().isNull()) {
					INode<T, V> y = x.getParent();
					while (!y.isNull() && y.getRightChild() == x) {
						x = y;
						y = x.getParent();
					}
					if (y.isNull()) {
						return null;
					}
					return new EntryImp<T, V>(y.getKey(), y.getValue());
				}
				x = x.getRightChild();
			} else if (key.compareTo(x.getKey()) < 0) {
				if (x.getLeftChild().isNull()) {
					return new EntryImp<T, V>(x.getKey(), x.getValue());
				}
				x = x.getLeftChild();
			} else {
				return new EntryImp<T, V>(x.getKey(), x.getValue());
			}
		}
		return null;
	}

	@Override
	public T ceilingKey(T key) {
		if (key == null) {
			throw new RuntimeErrorException(null);
		}
		Entry<T, V> x = ceilingEntry(key);
		return x == null ? null : x.getKey();
	}

	@Override
	public void clear() {
		redBlackTree.clear();
		size = 0;

	}

	@Override
	public boolean containsKey(T key) {
		if (key == null) {
			throw new RuntimeErrorException(null);
		}
		return redBlackTree.contains(key);
	}

	@Override
	public boolean containsValue(V value) {
		if (value == null) {
			throw new RuntimeErrorException(null);
		}
		return dfsSearch(value, redBlackTree.getRoot());
	}

	private boolean dfsSearch(V value, INode<T, V> x) {
		if (x.isNull()) {
			return false;
		}
		if (x.getValue().equals(value)) {
			return true;
		}
		return dfsSearch(value, x.getLeftChild()) || dfsSearch(value, x.getRightChild());
	}

	@Override
	public Set<Entry<T, V>> entrySet() {
		Set<Entry<T, V>> entrySet = new LinkedHashSet<>();
		fillEntrySet(entrySet, redBlackTree.getRoot());
		return entrySet;
	}

	private void fillEntrySet(Set<Entry<T, V>> entrySet, INode<T, V> x) {
		if (x.isNull()) {
			return;
		}

		fillEntrySet(entrySet, x.getLeftChild());
		entrySet.add(new EntryImp<T, V>(x.getKey(), x.getValue()));
		fillEntrySet(entrySet, x.getRightChild());
	}

	@Override
	public Entry<T, V> firstEntry() {
		INode<T, V> x = redBlackTree.getRoot();
		while (!x.isNull() && !x.getLeftChild().isNull()) {
			x = x.getLeftChild();
		}
		return x.isNull() ? null : new EntryImp<T, V>(x.getKey(), x.getValue());
	}

	@Override
	public T firstKey() {
		Entry<T, V> x = firstEntry();
		return x == null ? null : x.getKey();
	}

	@Override
	public Entry<T, V> floorEntry(T key) {
		if (key == null) {
			throw new RuntimeErrorException(null);
		}
		INode<T, V> x = redBlackTree.getRoot();
		while (!x.isNull()) {
			if (key.compareTo(x.getKey()) > 0) {
				if (x.getRightChild().isNull()) {
					return new EntryImp<T, V>(x.getKey(), x.getValue());
				}
				x = x.getRightChild();
			} else if (key.compareTo(x.getKey()) < 0) {
				if (x.getLeftChild().isNull()) {
					INode<T, V> y = x.getParent();
					while (!y.isNull() && y.getLeftChild() == x) {
						x = y;
						y = x.getParent();
					}
					if (y.isNull()) {
						return null;
					}
					return new EntryImp<T, V>(y.getKey(), y.getValue());
				}
				x = x.getLeftChild();
			} else {
				return new EntryImp<T, V>(x.getKey(), x.getValue());
			}
		}
		return null;
	}

	@Override
	public T floorKey(T key) {
		if (key == null) {
			throw new RuntimeErrorException(null);
		}
		Entry<T, V> x = floorEntry(key);
		return x == null ? null : x.getKey();
	}

	@Override
	public V get(T key) {
		if (key == null) {
			throw new RuntimeErrorException(null);
		}
		return redBlackTree.search(key);
	}

	@Override
	public ArrayList<Entry<T, V>> headMap(T toKey) {
		return headMap(toKey, false);
	}

	@Override
	public ArrayList<Entry<T, V>> headMap(T toKey, boolean inclusive) {
		if (toKey == null) {
			throw new RuntimeErrorException(null);
		}
		ArrayList<Entry<T, V>> data = new ArrayList<>();
		searchTraversal(data, redBlackTree.getRoot(), toKey, inclusive);
		return data;
	}

	private void searchTraversal(ArrayList<Entry<T, V>> data, INode<T, V> x, T toKey, boolean inclusive) {
		if (x.isNull() || x == null) {
			return;
		}
		if (x.getKey().compareTo(toKey) >= 0) {
			searchTraversal(data, x.getLeftChild(), toKey, inclusive);
			if (inclusive && toKey.equals(x.getKey())) {
				data.add(new EntryImp<T, V>(x.getKey(), x.getValue()));
			}
		} else {
			searchTraversal(data, x.getLeftChild(), toKey, inclusive);
			data.add(new EntryImp<T, V>(x.getKey(), x.getValue()));
			searchTraversal(data, x.getRightChild(), toKey, inclusive);
		}
	}

	@Override
	public Set<T> keySet() {
		Set<T> data = new TreeSet<>();
		ArrayList<INode<T, V>> temp = new ArrayList<>();
		inorderTraversal(temp, redBlackTree.getRoot());
		for (INode<T, V> node : temp) {
			data.add(node.getKey());
		}
		return data;
	}

	@Override
	public Entry<T, V> lastEntry() {
		if (redBlackTree.isEmpty()) {
			return null;
		}
		INode<T, V> x = redBlackTree.getRoot();
		while (!x.getRightChild().isNull()) {
			x = x.getRightChild();
		}
		return new EntryImp<T, V>(x.getKey(), x.getValue());
	}

	@Override
	public T lastKey() {
		Entry<T, V> x = lastEntry();
		if (x != null) {
			return lastEntry().getKey();
		} else {
			return null;
		}
	}

	@Override
	public Entry<T, V> pollFirstEntry() {
		Entry<T, V> x = firstEntry();
		if (x != null) {
			remove(x.getKey());
		}
		return x;
	}

	@Override
	public Entry<T, V> pollLastEntry() {
		Entry<T, V> x = lastEntry();
		if (x != null) {
			remove(x.getKey());
		}
		return x;

	}

	@Override
	public void put(T key, V value) {
		boolean contain = containsKey(key);
		redBlackTree.insert(key, value);
		if (!contain) {
			size++;
		}
	}

	@Override
	public void putAll(Map<T, V> map) {
		if (map == null) {
			throw new RuntimeErrorException(null);
		}
		for (Map.Entry<T, V> x : map.entrySet()) {
			put(x.getKey(), x.getValue());
		}
	}

	@Override
	public boolean remove(T key) {
		if (key == null) {
			throw new RuntimeErrorException(null);
		}

		try {
			redBlackTree.delete(key);
			size--;
			return true;
		} catch (RuntimeErrorException e) {
			return false;
		}
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public Collection<V> values() {

		Collection<V> data = new ArrayList<>();
		ArrayList<INode<T, V>> temp = new ArrayList<>();
		inorderTraversal(temp, redBlackTree.getRoot());
		for (INode<T, V> node : temp) {
			data.add(node.getValue());
		}
		return data;
	}

	private void inorderTraversal(ArrayList<INode<T, V>> arr, INode<T, V> x) {
		if (x.isNull() || x == null) {
			return;
		}
		inorderTraversal(arr, x.getLeftChild());
		arr.add(x);
		inorderTraversal(arr, x.getRightChild());
	}

}
